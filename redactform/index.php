<?php

require_once "../lib/function.php";

     $categoryi     = $_POST["category"];
     $ad_type       = $_POST["ad_type"];
     $title         = htmlspecialchars(trim($_POST["title"]));
     $description   = htmlspecialchars(trim($_POST["description"]));
     $phone_number  = htmlspecialchars(trim($_POST["phone_number"]));
     $redact_id     = $_POST["redact_stat_id"];
     $success       = true;

if ( !empty($_POST["title"]) && !empty($_POST["description"]) && !empty($_POST["phone_number"]) ) {

    if (!$success) {
        $alert = "Սխալ հայտարարություն";
        include "./alert.php";
    } else {
        redact_Update_Statement($categoryi, $ad_type, $title, $description, $phone_number, $redact_id);
        $alert = "Ձեր հայտարարությունը փոփոխվել է";
        $_POST="";
        header("Location: ".$_SERVER["HTTP_REFERER"]."&&red=1");
    }

    /* $img = $_FILES['img'];

     if(!empty($img))
     {
         $img_desc = reArrayFiles($img);
         $i=1;
         foreach($img_desc as $val)
         {
             $newname[$i] = 'nkar' . $users_id."_".$i.".jpg";
             move_uploaded_file($val['tmp_name'],'./files/'.$newname[$i]);
             $i++;
         }
         Get_user_img_add ($state_id,$newname);
     }
*/
}




?>