<a href="./addStatments.php">
    <div class="my-page-statement-button box-s">
        + Ավելացնել հայտարարություն
    </div>
</a>
<?php
if (isset($_SESSION["email"])) {
    $users_id = GetUser_Id($_SESSION["email"]);
    $users_id = $users_id[0]['id'];
    $user_id_statement = GetAllStatements_user_id($users_id);

    ?>
    <div class="my-page-my-statement">
        <span>Իմ Հայտարարություները</span>
        <?php

        for ($i = 0; $i < count($user_id_statement); $i++) {
            $id = $user_id_statement[$i]["id"];
            $title = $user_id_statement[$i]["title"];
            $description = $user_id_statement[$i]["description"];
            $image_name = Get_user_img_add_statemant($id);
            ?>
            <div class="type-statement-block my-page-type-statement box-s">
                <a href="/korcrel.am/redactStatement.php?redact_statment_id=<?php echo $id; ?>"">
                <div class="popoxel box-s">
                    <i class="fa fa-pencil-square" aria-hidden="true"></i>

                </div>
                </a>
                <div class="type-statement-img box-s">
                    <a href="/korcrel.am/statementPage.php?statment_number=<?php echo $id; ?>">
                        <img src="/korcrel.am/files/<?php echo $image_name[0]["img_1"]; ?>">
                    </a>
                </div>
                <div class="type-statement-name box-s">
                    <a href="/korcrel.am/statementPage.php?statment_number=<?php echo $id; ?>">
                        <?php echo $title . "</br>"; ?>
                        <?php echo "<p>" . $description . "</p>"; ?>
                    </a>
                </div>
            </div>
        <?php }; ?>
    </div>

<?php }; ?>