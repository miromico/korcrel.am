function menuDisplay(itemId) {
    var childes = document.getElementById(itemId);
    if(getComputedStyle(childes).display != "none") {
        childes.style.display = "none";
    } else {
        childes.style.display = "block";
    }
}