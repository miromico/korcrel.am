<?php
    global $category;
    $category = GetAllCategory();
?>
<section class="section box-s" id="section1">
    <nav>
        <ul class="menu" id="menu">
            <?php
                for ($i = 0; $i < count($category); $i++) {
                    $title = $category[$i]["title"];
            ?>
                    <li class="has-child">
                    <a href="<?=$siteUrl."category/".$category[$i]["link"]."/"?>">
                        <span>
                            <?php
                                echo $title;
                            ?>
                        </span>
                    </a>
                    </li>
            <?php 
                }
            ?>
        </ul>
    </nav>
</section>
