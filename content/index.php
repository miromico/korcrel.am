<div class="statement-container box-s">
    <div class="main-statement-text-new box-s">
        Նոր հայտարարություներ
    </div>

    <?php

        $count = count(GetAllStatements_front());
        for ($i = 0; $i < $count; $i++) {
            $title= GetAllStatements_front();
            $statment_id=$title[$i]["id"]; 
            $image_name = Get_user_img_add_statemant($statment_id);
            ?>

            <div class="main-statement-block box-s" id="<?php echo $statment_id; ?>">
                <div class="main-statement-img" >
                    <a href="/korcrel.am/statementPage.php?statment_number=<?php echo $statment_id; ?>">
                        <img src="/korcrel.am/files/<?php echo $image_name[0]["img_1"];  ?>">
                    </a>
                </div>
                <div class="main-statement-name">
                    <a href="/korcrel.am/statementPage.php?statment_number=<?php echo $statment_id; ?>">
                        <?php echo $title[$i]["title"]; ?>
                    </a>
                </div>
            </div>
        <?php }
        ?>
</div>