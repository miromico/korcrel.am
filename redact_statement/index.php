<?php


$redact_id= $_GET["redact_statment_id"];
$redact_id_stat=GetRedactStatement($redact_id);

$users_id = GetUser_Id($_SESSION["email"]);
$users_id = $users_id[0]['id'];

$category = GetAllCategory();
$search_type = GetAllSearch_type();

?>

<div class="addstatment-text"> Փոփոխել հայտարարություն</div>
<form method="post" name="newStatement" enctype="multipart/form-data" action="redactform/index.php">
    <div class="yntrel">
            <?php
            if (!empty($_GET["red"]) && $_GET["red"]=='1'){
                ?>
                <div class="inf_add_stat box-s">  Ձեր հայտարարությունը փոփոխվել է</div>

                <?php

            }

            ?>
        <?php
        for ($i = 0; $i < count($search_type); $i++) {
            $id = $search_type[$i]["id"];
            $title = $search_type[$i]["title"];
            if ( $redact_id_stat[0]['search_type'] == $id) {
                ?>
                <label>
                    <input name="ad_type" id="ad_type_<?php echo $id; ?> " type="radio" value="<?php echo $id ?>"
                           checked="">
                    <?php
                    echo $title;
                    ?>
                </label>
            <?php } else { ?>
                <label>
                    <input name="ad_type" id="ad_type_<?php echo $id; ?> " type="radio" value="<?php echo $id ?>">
                    <?php
                    echo $title;
                    ?>
                </label>
            <?php } ?>
        <?php } ?>


    </div>
    <div class="addstatment-type">
        <span>
            Տեսակը
        </span><br>
        <select name="category">
            <?php
            for ($i = 0; $i < count($category); $i++) {
                $id = $category[$i]["id"];
                $title = $category[$i]["title"];
                    if ( $redact_id_stat[0]['category'] == $id) {
                ?>
                    <option selected value="<?php echo $id; ?>">
                        <?php
                            echo $title;
                        ?>
                    </option>
                <?php } else { ?>
                    <option value="<?php echo $id; ?>">
                        <?php
                        echo $title;
                        ?>
                    </option>
                 <?php } ?>
            <?php } ?>
        </select>
    </div>
    <div class="add-statement-name">
        <div class="fsec">
            <div class="flabel">
                <label for="_idtitle">
                    Անվանում
                </label>
            </div>
            <div class="felement">
                <input maxlength="100" name="title" id="_idtitle" type="text" value="<?php echo $redact_id_stat[0]['title']; ?>">
            </div>
        </div>
    </div>
    <div class="nkaragir">
        <div class="flabel">
            <label for="_iddescription">
                Նկարագիր
            </label>
        </div>

        <div class="felement">
        <textarea rows="8" cols="50" name="description" id="_iddescription" ><?php echo $redact_id_stat[0]['description']; ?></textarea>
        </div>
    </div>

    <div class="statement-photo">
        <div class="flabel">
            Լուսանկարներ
        </div>
        <div class="statement-photo-tex">

        </div>
        <div class="addPhoto">
            <input type="file" value="Ավելացնել լուսանկար"  multiple="multiple" name="img[]">
        </div>
    </div>


    <div class="add-statement-number">
        <div class="flabel">
            <label for="_idphone_numbers">
                Հեռախոսահամար
            </label>
        </div>
        <div class="felement">
            <input class="phone" type="tel" name="phone_number" id="_idphone_numbers" value="<?php echo $redact_id_stat[0]['number']; ?>"></div>
    </div> 
    <input type="hidden" value="<?php echo $redact_id; ?>" name="redact_stat_id">
    <div class="addstatment-block">
        <input type="submit" class="addstatment" value="Պահպանել" name="addStatement">
    </div>
</form>