<?php
require_once "./start.php"

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>korcrel.am.am</title>
    <link rel="stylesheet" type="text/css" href="/korcrel.am/css/fonts.css"/>
    <link rel="stylesheet" type="text/css" href="/korcrel.am/css/index.css"/>
    <link rel="stylesheet" type="text/css" href="/korcrel.am/registration/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/footer/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/menu/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/header/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/content/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/myPage/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/mobileMenu/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/singIn/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/addStatments/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/statmentPage/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/typeStatementPage/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/myPageStatement/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/myPageSetting/index.css">
    <link rel="stylesheet" type="text/css" href="/korcrel.am/typeStatementMenu/index.css">


</head>
<body>
<header class="header">
    <div class="wrapper">
        <a href="/korcrel.am" ><div class="logo"></div></a>
        <div class="mini-menu-block" onclick="mobileMenuDisplay('section1')">
            <div class="mini-menu-icon">
                <i class="fa fa-th-large" aria-hidden="true"></i>

            </div>
        </div>

        <div class="search-block">
            <input id="search" class="search" type="search">
            <div class="search-icon">
                <label for="search">
                    <i class="fa fa-search"></i>
                </label>
            </div>
        </div>
<div class="bar-block" onclick="rigthMobileMenuDisplay('header-menu1')">
    <div class="bar-icon ">
        <i class="fa fa-bars" aria-hidden="true"></i>

        </div>

</div>
        <div class="header-menu box-s" id="header-menu1">
            <span class="my-page button box-s">
                <a href="/korcrel.am/myMainPage.php"> Իմ էջը
                </a>
            </span>
            <span class="add-statement button box-s">
                <a href="/korcrel.am/addStatments.php">
                    Ավելացնել հայտարարություն
                </a>
            </span>
        </div>
    </div>
</header>