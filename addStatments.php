<?php


include_once "header/index.php";
include_once "menu/index.php";
?>
    <div class="site-content box-s">
        <?php
        if (!empty($_SESSION["email"]) && !empty($_SESSION["password"]) && checkUser($_SESSION["email"], $_SESSION["password"])) {
            include_once "addStatments/index.php";
        }
        else{
            include_once "singIn/index.php";
        }

        ?>
    </div>
<?php
include_once "footer/index.php";