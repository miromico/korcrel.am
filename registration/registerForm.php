<?php
require_once "../lib/function.php";

if (!empty($_POST["button_reg"])) {
    $email = htmlspecialchars($_POST["email"]);
    $password_1 = htmlspecialchars($_POST["password_1"]);
    $password_2 = htmlspecialchars($_POST["password_2"]);
    $number = htmlspecialchars($_POST["number"]);

    $success = true;

    if (strlen($email) < 3) {
        $success = false;
    }
    elseif (strlen($number)  <3) {
        $success = false;
    } elseif ($password_1 != $password_2) {
        $success = false;
    }

    if (!$success) {
        header("Location: ".$_SERVER["HTTP_REFERER"]."?reg=0");

        exit();
    } else {
        addUser($email, md5($password_1), $number);
        header("Location: ../singIn.php?reg=1");
        exit();

    }




}
?>